# Join

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**user** | **string** |  | 
**server** | **string** |  | 
**insertDate** | [**\DateTime**](\DateTime.md) |  | 
**bounced** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



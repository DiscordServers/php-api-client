# Server2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | 
**owner** | **string** |  | [optional] 
**ownerName** | **string** |  | [optional] 
**name** | **string** |  | 
**keywords** | **string[]** |  | [optional] 
**mainKeyword** | **string** |  | [optional] 
**categories** | **string[]** |  | [optional] 
**region** | **string** |  | [optional] 
**nsfwLevel** | **int** |  | [optional] 
**nsfw** | **bool** |  | [optional] 
**icon** | **string** |  | [optional] 
**splash** | **string** |  | [optional] 
**links** | [**\DiscordServers\ApiClient\Model\LinkForm[]**](LinkForm.md) |  | 
**description** | **string** |  | [optional] 
**longDescription** | **string** |  | [optional] 
**customInvite** | **string** |  | [optional] 
**enabled** | **bool** |  | 
**listed** | **bool** |  | 
**hasBot** | **bool** |  | 
**members** | **int** |  | [optional] 
**insertDate** | [**\DateTime**](\DateTime.md) |  | 
**updateDate** | [**\DateTime**](\DateTime.md) |  | 
**featured** | **bool** |  | 
**premium** | **bool** |  | [optional] 
**sponsor** | **bool** |  | [optional] 
**sponsorLevel** | **string** |  | [optional] 
**gems** | **int** |  | [optional] 
**freeGems** | **int** |  | [optional] 
**paidGems** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



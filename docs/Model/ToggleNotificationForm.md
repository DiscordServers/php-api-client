# ToggleNotificationForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **string** |  | 
**enabled** | **float** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



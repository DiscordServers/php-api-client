# UserNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**category** | **string** |  | 
**enabled** | **bool** |  | 
**insertDate** | [**\DateTime**](\DateTime.md) |  | 
**updateDate** | [**\DateTime**](\DateTime.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**email** | **string** |  | [optional] 
**apiKey** | **string** |  | [optional] 
**chargebeeCustomerId** | **string** |  | [optional] 
**updateDate** | [**\DateTime**](\DateTime.md) |  | 
**insertDate** | [**\DateTime**](\DateTime.md) |  | 
**notifications** | [**\DiscordServers\ApiClient\Model\UserNotification[]**](UserNotification.md) |  | 
**autoBumpServers** | **string[]** |  | [optional] 
**elite** | **bool** |  | [optional] 
**dmChannelId** | **string** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**balance** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



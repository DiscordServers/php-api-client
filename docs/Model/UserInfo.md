# UserInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**\DiscordServers\ApiClient\Model\User**](User.md) |  | 
**servers** | [**\DiscordServers\ApiClient\Model\Server[]**](Server.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



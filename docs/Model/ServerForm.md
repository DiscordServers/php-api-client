# ServerForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner** | **string** |  | [optional] 
**ownerName** | **string** |  | [optional] 
**identifier** | **string** |  | 
**name** | **string** |  | [optional] 
**keywords** | **string[]** |  | [optional] 
**categories** | **string[]** |  | [optional] 
**links** | [**\DiscordServers\ApiClient\Model\LinkForm[]**](LinkForm.md) |  | [optional] 
**mainKeyword** | **string** |  | [optional] 
**region** | **string** |  | [optional] 
**icon** | **string** |  | [optional] 
**customInvite** | **string** |  | [optional] 
**members** | **int** |  | [optional] 
**splash** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**longDescription** | **string** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**listed** | **bool** |  | [optional] 
**hasBot** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



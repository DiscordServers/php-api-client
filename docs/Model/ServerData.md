# ServerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**servers** | [**\DiscordServers\ApiClient\Model\ServerForm[]**](ServerForm.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



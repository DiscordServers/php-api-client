# TokenSale

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**user** | [**\DiscordServers\ApiClient\Model\User**](User.md) |  | 
**amount** | **int** |  | 
**free** | **bool** |  | [optional] 
**automatic** | **bool** |  | [optional] 
**insertDate** | [**\DateTime**](\DateTime.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# DiscordServers\ApiClient\CustomerApi

All URIs are relative to *https://api.discordservers.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](CustomerApi.md#get) | **GET** /customer/{customer} | Fetch a Customer
[**register**](CustomerApi.md#register) | **POST** /customer/ | Register


# **get**
> \DiscordServers\ApiClient\Model\Customer get($customer)

Fetch a Customer

Get a customer by customer id

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customer = 56; // int | Customer ID

try {
    $result = $apiInstance->get($customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer** | **int**| Customer ID |

### Return type

[**\DiscordServers\ApiClient\Model\Customer**](../Model/Customer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **register**
> \DiscordServers\ApiClient\Model\Customer2 register($registration)

Register

Register with username and password

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$registration = new \DiscordServers\ApiClient\Model\RegistrationForm(); // \DiscordServers\ApiClient\Model\RegistrationForm | Registration form

try {
    $result = $apiInstance->register($registration);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->register: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registration** | [**\DiscordServers\ApiClient\Model\RegistrationForm**](../Model/RegistrationForm.md)| Registration form |

### Return type

[**\DiscordServers\ApiClient\Model\Customer2**](../Model/Customer2.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


# DiscordServers\ApiClient\ServerApi

All URIs are relative to *https://api.discordservers.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCategory**](ServerApi.md#addCategory) | **POST** /server/{server}/category | Add a category
[**addJoin**](ServerApi.md#addJoin) | **POST** /server/{server}/join/{userId} | Add a Join
[**addKeyword**](ServerApi.md#addKeyword) | **POST** /server/{server}/keyword | Add a keyword
[**bulkUpsert**](ServerApi.md#bulkUpsert) | **POST** /server/bulk | Bulk Upsert server
[**callList**](ServerApi.md#callList) | **GET** /server | List servers
[**delete**](ServerApi.md#delete) | **DELETE** /server/{server} | Delete a server
[**get**](ServerApi.md#get) | **GET** /server/{server} | Fetch a server
[**getActiveServers**](ServerApi.md#getActiveServers) | **GET** /server/active | Get Active Servers
[**hasBot**](ServerApi.md#hasBot) | **GET** /server/{server}/bot | Check for bot
[**isDelisted**](ServerApi.md#isDelisted) | **GET** /server/{server}/delisted | Check if a server is delisted
[**isValidInvite**](ServerApi.md#isValidInvite) | **GET** /server/{server}/isValidInvite | Checks valid invite
[**remove**](ServerApi.md#remove) | **POST** /server/{server}/remove | Remove own server
[**removeCategory**](ServerApi.md#removeCategory) | **DELETE** /server/{server}/category | Remove a category
[**removeKeyword**](ServerApi.md#removeKeyword) | **DELETE** /server/{server}/keyword | Remove a keyword
[**upsert**](ServerApi.md#upsert) | **POST** /server/upsert | Upsert server


# **addCategory**
> \DiscordServers\ApiClient\Model\Server addCategory($server, $category)

Add a category

Adds a category to a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server ID
$category = new \DiscordServers\ApiClient\Model\AddRemoveForm(); // \DiscordServers\ApiClient\Model\AddRemoveForm | Category to Add

try {
    $result = $apiInstance->addCategory($server, $category);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->addCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |
 **category** | [**\DiscordServers\ApiClient\Model\AddRemoveForm**](../Model/AddRemoveForm.md)| Category to Add |

### Return type

[**\DiscordServers\ApiClient\Model\Server**](../Model/Server.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addJoin**
> \DiscordServers\ApiClient\Model\Join addJoin($server, $userId)

Add a Join

Add a join for a server.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server ID
$userId = 56; // int | User ID

try {
    $result = $apiInstance->addJoin($server, $userId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->addJoin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |
 **userId** | **int**| User ID |

### Return type

[**\DiscordServers\ApiClient\Model\Join**](../Model/Join.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addKeyword**
> \DiscordServers\ApiClient\Model\Server addKeyword($server, $keyword)

Add a keyword

Adds a keyword to a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server ID
$keyword = new \DiscordServers\ApiClient\Model\AddRemoveForm(); // \DiscordServers\ApiClient\Model\AddRemoveForm | Keyword to Add

try {
    $result = $apiInstance->addKeyword($server, $keyword);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->addKeyword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |
 **keyword** | [**\DiscordServers\ApiClient\Model\AddRemoveForm**](../Model/AddRemoveForm.md)| Keyword to Add |

### Return type

[**\DiscordServers\ApiClient\Model\Server**](../Model/Server.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **bulkUpsert**
> \DiscordServers\ApiClient\Model\InlineResponse500 bulkUpsert($serverData)

Bulk Upsert server

Bulk Upserts servers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$serverData = new \DiscordServers\ApiClient\Model\ServerData(); // \DiscordServers\ApiClient\Model\ServerData | Server data to edit

try {
    $result = $apiInstance->bulkUpsert($serverData);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->bulkUpsert: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverData** | [**\DiscordServers\ApiClient\Model\ServerData**](../Model/ServerData.md)| Server data to edit | [optional]

### Return type

[**\DiscordServers\ApiClient\Model\InlineResponse500**](../Model/InlineResponse500.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **callList**
> callList($size, $from, $fields, $filter)

List servers

List servers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$size = 5000; // int | Number of servers
$from = 0; // int | Number of servers
$fields = array("fields_example"); // string[] | Fields to grab
$filter = array("filter_example"); // string[] | Filters for the query

try {
    $apiInstance->callList($size, $from, $fields, $filter);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->callList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**| Number of servers | [optional] [default to 5000]
 **from** | **int**| Number of servers | [optional] [default to 0]
 **fields** | [**string[]**](../Model/string.md)| Fields to grab | [optional]
 **filter** | [**string[]**](../Model/string.md)| Filters for the query | [optional]

### Return type

void (empty response body)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **delete**
> \DiscordServers\ApiClient\Model\Server delete($server, $hard)

Delete a server

Deletes a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server ID
$hard = "hard_example"; // string | If set, will delete the record from the database

try {
    $result = $apiInstance->delete($server, $hard);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |
 **hard** | **string**| If set, will delete the record from the database | [optional]

### Return type

[**\DiscordServers\ApiClient\Model\Server**](../Model/Server.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **get**
> \DiscordServers\ApiClient\Model\Server2 get($server)

Fetch a server

Get a server by server id

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server = "server_example"; // string | Server ID

try {
    $result = $apiInstance->get($server);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |

### Return type

[**\DiscordServers\ApiClient\Model\Server2**](../Model/Server2.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getActiveServers**
> string[] getActiveServers($from, $size)

Get Active Servers

Fetches a list of all active servers.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$from = 0; // int | Start point
$size = 5000; // int | Number of records to grab

try {
    $result = $apiInstance->getActiveServers($from, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->getActiveServers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **int**| Start point | [default to 0]
 **size** | **int**| Number of records to grab | [default to 5000]

### Return type

**string[]**

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **hasBot**
> \DiscordServers\ApiClient\Model\InlineResponse2001 hasBot($server)

Check for bot

Checks whether or not a server has the bot

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server = "server_example"; // string | Server ID

try {
    $result = $apiInstance->hasBot($server);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->hasBot: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |

### Return type

[**\DiscordServers\ApiClient\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **isDelisted**
> \DiscordServers\ApiClient\Model\InlineResponse2002 isDelisted($server)

Check if a server is delisted

Checks whether or not a server is delisted

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server = "server_example"; // string | Server ID

try {
    $result = $apiInstance->isDelisted($server);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->isDelisted: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |

### Return type

[**\DiscordServers\ApiClient\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **isValidInvite**
> \DiscordServers\ApiClient\Model\InlineResponse2003 isValidInvite($server, $code)

Checks valid invite

Checks to see if an invite is Valid

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server = "server_example"; // string | Server ID
$code = "code_example"; // string | Invite Code

try {
    $result = $apiInstance->isValidInvite($server, $code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->isValidInvite: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |
 **code** | **string**| Invite Code |

### Return type

[**\DiscordServers\ApiClient\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **remove**
> \DiscordServers\ApiClient\Model\Server remove($server)

Remove own server

Removes a user's server by server id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server ID

try {
    $result = $apiInstance->remove($server);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->remove: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |

### Return type

[**\DiscordServers\ApiClient\Model\Server**](../Model/Server.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeCategory**
> \DiscordServers\ApiClient\Model\Server removeCategory($server, $category)

Remove a category

Removes a category to a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server ID
$category = new \DiscordServers\ApiClient\Model\AddRemoveForm(); // \DiscordServers\ApiClient\Model\AddRemoveForm | Category to Remove

try {
    $result = $apiInstance->removeCategory($server, $category);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->removeCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |
 **category** | [**\DiscordServers\ApiClient\Model\AddRemoveForm**](../Model/AddRemoveForm.md)| Category to Remove |

### Return type

[**\DiscordServers\ApiClient\Model\Server**](../Model/Server.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeKeyword**
> \DiscordServers\ApiClient\Model\Server removeKeyword($server, $keyword)

Remove a keyword

Removes a keyword to a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server ID
$keyword = new \DiscordServers\ApiClient\Model\AddRemoveForm(); // \DiscordServers\ApiClient\Model\AddRemoveForm | Keyword to Remove

try {
    $result = $apiInstance->removeKeyword($server, $keyword);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->removeKeyword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server ID |
 **keyword** | [**\DiscordServers\ApiClient\Model\AddRemoveForm**](../Model/AddRemoveForm.md)| Keyword to Remove |

### Return type

[**\DiscordServers\ApiClient\Model\Server**](../Model/Server.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **upsert**
> \DiscordServers\ApiClient\Model\Server upsert($data)

Upsert server

Upserts a server by server id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\ServerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \DiscordServers\ApiClient\Model\ServerForm(); // \DiscordServers\ApiClient\Model\ServerForm | Server data to edit

try {
    $result = $apiInstance->upsert($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServerApi->upsert: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\DiscordServers\ApiClient\Model\ServerForm**](../Model/ServerForm.md)| Server data to edit |

### Return type

[**\DiscordServers\ApiClient\Model\Server**](../Model/Server.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


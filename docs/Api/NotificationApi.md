# DiscordServers\ApiClient\NotificationApi

All URIs are relative to *https://api.discordservers.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllForUser**](NotificationApi.md#getAllForUser) | **GET** /notification/{usr} | Get User Notifications
[**prepare**](NotificationApi.md#prepare) | **POST** /notification/prepare/{bucket} | Prepare a notification for sending to users
[**signup**](NotificationApi.md#signup) | **POST** /notification/{usr}/signup | Sign Up for User Notification
[**toggle**](NotificationApi.md#toggle) | **POST** /notification/{usr} | Toggle User Notification


# **getAllForUser**
> getAllForUser($user, $usr)

Get User Notifications

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\NotificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = 8.14; // float | User ID
$usr = "usr_example"; // string | 

try {
    $apiInstance->getAllForUser($user, $usr);
} catch (Exception $e) {
    echo 'Exception when calling NotificationApi->getAllForUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **float**| User ID |
 **usr** | **string**|  |

### Return type

void (empty response body)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **prepare**
> prepare($bucket, $content, $test)

Prepare a notification for sending to users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\NotificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$bucket = "bucket_example"; // string | Bucket to send to
$content = "content_example"; // string | Message content to send
$test = true; // bool | If set, will only send messages to Aaron and Arunesh

try {
    $apiInstance->prepare($bucket, $content, $test);
} catch (Exception $e) {
    echo 'Exception when calling NotificationApi->prepare: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bucket** | **string**| Bucket to send to |
 **content** | **string**| Message content to send |
 **test** | **bool**| If set, will only send messages to Aaron and Arunesh | [optional]

### Return type

void (empty response body)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: text/markdown
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **signup**
> signup($user, $usr)

Sign Up for User Notification

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\NotificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | User ID
$usr = "usr_example"; // string | 

try {
    $apiInstance->signup($user, $usr);
} catch (Exception $e) {
    echo 'Exception when calling NotificationApi->signup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| User ID |
 **usr** | **string**|  |

### Return type

void (empty response body)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **toggle**
> \DiscordServers\ApiClient\Model\UserNotification toggle($user, $data, $usr)

Toggle User Notification

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\NotificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | User ID
$data = new \DiscordServers\ApiClient\Model\ToggleNotificationForm(); // \DiscordServers\ApiClient\Model\ToggleNotificationForm | Toggle Data data
$usr = "usr_example"; // string | 

try {
    $result = $apiInstance->toggle($user, $data, $usr);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationApi->toggle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| User ID |
 **data** | [**\DiscordServers\ApiClient\Model\ToggleNotificationForm**](../Model/ToggleNotificationForm.md)| Toggle Data data |
 **usr** | **string**|  |

### Return type

[**\DiscordServers\ApiClient\Model\UserNotification**](../Model/UserNotification.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


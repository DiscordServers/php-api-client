# DiscordServers\ApiClient\TokenApi

All URIs are relative to *https://api.discordservers.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAvailableTokensForUser**](TokenApi.md#getAvailableTokensForUser) | **GET** /token/user/{user} | Get Available Tokens for User
[**getLastSale**](TokenApi.md#getLastSale) | **GET** /token/{server}/{user}/last | Get Last Token Sale of a User for a Server
[**getTokenConfig**](TokenApi.md#getTokenConfig) | **GET** /token/config | Get Token Config
[**giveTokens**](TokenApi.md#giveTokens) | **POST** /token/give/{server}/{usr}/{amount} | Give tokens to a server


# **getAvailableTokensForUser**
> float getAvailableTokensForUser($user)

Get Available Tokens for User

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\TokenApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user = "user_example"; // string | 

try {
    $result = $apiInstance->getAvailableTokensForUser($user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokenApi->getAvailableTokensForUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**|  |

### Return type

**float**

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getLastSale**
> \DiscordServers\ApiClient\Model\TokenSale getLastSale($server, $user)

Get Last Token Sale of a User for a Server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\TokenApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server to check sales
$user = "user_example"; // string | User to check

try {
    $result = $apiInstance->getLastSale($server, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokenApi->getLastSale: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server to check sales |
 **user** | **string**| User to check |

### Return type

[**\DiscordServers\ApiClient\Model\TokenSale**](../Model/TokenSale.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTokenConfig**
> object getTokenConfig()

Get Token Config

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\TokenApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getTokenConfig();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokenApi->getTokenConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **giveTokens**
> \DiscordServers\ApiClient\Model\TokenSale giveTokens($server, $usr, $amount, $free)

Give tokens to a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\TokenApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$server = "server_example"; // string | Server to add gems to
$usr = "usr_example"; // string | User giving the gems
$amount = 8.14; // float | Number of gems to give
$free = true; // bool | Is this for free?

try {
    $result = $apiInstance->giveTokens($server, $usr, $amount, $free);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokenApi->giveTokens: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **string**| Server to add gems to |
 **usr** | **string**| User giving the gems |
 **amount** | **float**| Number of gems to give |
 **free** | **bool**| Is this for free? | [optional]

### Return type

[**\DiscordServers\ApiClient\Model\TokenSale**](../Model/TokenSale.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


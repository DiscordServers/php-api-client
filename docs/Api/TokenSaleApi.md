# DiscordServers\ApiClient\TokenSaleApi

All URIs are relative to *https://api.discordservers.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](TokenSaleApi.md#get) | **GET** /tokenSale/{tokenSale} | Get Token Sale


# **get**
> \DiscordServers\ApiClient\Model\TokenSale2 get($tokenSale)

Get Token Sale

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\TokenSaleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tokenSale = "tokenSale_example"; // string | 

try {
    $result = $apiInstance->get($tokenSale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokenSaleApi->get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tokenSale** | **string**|  |

### Return type

[**\DiscordServers\ApiClient\Model\TokenSale2**](../Model/TokenSale2.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


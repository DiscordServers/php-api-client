# DiscordServers\ApiClient\TokenInvoiceApi

All URIs are relative to *https://api.discordservers.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](TokenInvoiceApi.md#get) | **GET** /tokenInvoice/{tokenInvoice} | Get Token Invoice


# **get**
> \DiscordServers\ApiClient\Model\TokenInvoice get($tokenInvoice)

Get Token Invoice

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: ApiKey
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('X-API-Key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-Key', 'Bearer');
// Configure API key authorization: Bearer
$config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = DiscordServers\ApiClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new DiscordServers\ApiClient\Api\TokenInvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tokenInvoice = "tokenInvoice_example"; // string | 

try {
    $result = $apiInstance->get($tokenInvoice);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokenInvoiceApi->get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tokenInvoice** | **string**|  |

### Return type

[**\DiscordServers\ApiClient\Model\TokenInvoice**](../Model/TokenInvoice.md)

### Authorization

[ApiKey](../../README.md#ApiKey), [Bearer](../../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


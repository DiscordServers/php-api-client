# DiscordServers\ApiClient\ChargebeeApi

All URIs are relative to *https://api.discordservers.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getHostedPageUrl**](ChargebeeApi.md#getHostedPageUrl) | **GET** /chargebee/hostedPage | Get Hosted Page
[**getPortalSession**](ChargebeeApi.md#getPortalSession) | **GET** /chargebee/portalSession | Get Portal Session


# **getHostedPageUrl**
> object getHostedPageUrl($user, $email, $plan, $amount, $server)

Get Hosted Page

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\ChargebeeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$user = "user_example"; // string | User ID
$email = "email_example"; // string | User Email
$plan = "plan_example"; // string | Plan ID
$amount = 8.14; // float | Amount of gems
$server = "server_example"; // string | Server ID to apply the subscription to

try {
    $result = $apiInstance->getHostedPageUrl($user, $email, $plan, $amount, $server);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChargebeeApi->getHostedPageUrl: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **string**| User ID |
 **email** | **string**| User Email |
 **plan** | **string**| Plan ID |
 **amount** | **float**| Amount of gems |
 **server** | **string**| Server ID to apply the subscription to | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPortalSession**
> object getPortalSession()

Get Portal Session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\ChargebeeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPortalSession();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChargebeeApi->getPortalSession: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


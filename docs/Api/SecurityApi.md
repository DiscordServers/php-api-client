# DiscordServers\ApiClient\SecurityApi

All URIs are relative to *https://api.discordservers.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authenticate**](SecurityApi.md#authenticate) | **POST** /authenticate | Authentication
[**refresh**](SecurityApi.md#refresh) | **POST** /authenticate/refresh | Refresh Access Token


# **authenticate**
> \DiscordServers\ApiClient\Model\InlineResponse200 authenticate($username, $password)

Authentication

Login with username and password to get access and refresh tokens

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\SecurityApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$username = "username_example"; // string | Username to log in with
$password = "password_example"; // string | Password for the username

try {
    $result = $apiInstance->authenticate($username, $password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SecurityApi->authenticate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| Username to log in with |
 **password** | **string**| Password for the username |

### Return type

[**\DiscordServers\ApiClient\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refresh**
> \DiscordServers\ApiClient\Model\InlineResponse200 refresh($refreshToken)

Refresh Access Token

Refreshes your access token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new DiscordServers\ApiClient\Api\SecurityApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$refreshToken = "refreshToken_example"; // string | Refresh token

try {
    $result = $apiInstance->refresh($refreshToken);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SecurityApi->refresh: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refreshToken** | **string**| Refresh token |

### Return type

[**\DiscordServers\ApiClient\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

